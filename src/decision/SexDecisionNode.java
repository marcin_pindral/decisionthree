package decision;

import decision.events.Event;
import decision.events.SexDecisionAcceptEvent;
import decision.events.SexDecisionDeclineEvent;

import java.util.Optional;

public class SexDecisionNode extends DecisionNode {

    public SexDecisionNode(final DecisionTreeData decisionTreeData, final DecisionNode leftNode, final DecisionNode rightNode) {
        //TODO not null
        super(Optional.of(leftNode), Optional.of(rightNode));
        this.decisionTreeData = decisionTreeData;
    }

    @Override
    Event getAcceptEvent() {
        return SexDecisionAcceptEvent.getInstance();
    }

    @Override
    Event getDeclineEvent() {
        return SexDecisionDeclineEvent.getInstance();
    }

    @Override
    boolean verify() {
        return "M".equalsIgnoreCase(this.decisionTreeData.getSex());
    }
}
