package decision;

import decision.events.Event;

import java.util.Optional;

public abstract class DecisionNode implements Node {

    DecisionTreeData decisionTreeData;

    private Optional<DecisionNode> leftNode;
    private Optional<DecisionNode> rightNode;

    DecisionNode(final Optional<DecisionNode> leftNode, final Optional<DecisionNode> rightNode) {
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    @Override
    public boolean getDecision() {
        System.out.println("Current node: " + this.getClass().getSimpleName());
        boolean decision = this.verify();

        publishEvent(decision);

        if (isLastNode()) {
            return decision;
        }

        if (decision) {
            return this.leftNode.get()
                                .getDecision();
        }
        return this.rightNode.get()
                             .getDecision();
    }

    @Override
    public boolean isLastNode() {
        return !this.leftNode.isPresent() || !this.rightNode.isPresent();
    }

    @Override
    public void publishEvent(final boolean decision) {
        Event event;
        if (decision) {
            event = getAcceptEvent();
        } else {
            event = getDeclineEvent();
        }
        System.out.println("Published event: " + event.getClass().getSimpleName());
    }

    abstract Event getAcceptEvent();
    abstract Event getDeclineEvent();
    abstract boolean verify();
}
