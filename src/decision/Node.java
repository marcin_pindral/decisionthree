package decision;

public interface Node {

    boolean isLastNode();
    boolean getDecision();

    void publishEvent(boolean decision);
}
