# Opis

## Connected to the target VM, address: '127.0.0.1:37279', transport: 'socket'

## Start
## Loan identifier=1, Age=21, Sex=M, Marriage=true, Salary=1000, Profession=IT
## Current node: **ProfessionDecisionNode**
## Published event: **ProfessionDecisionAcceptEvent**
## Current node: **SalaryDecisionNode**
## Published event: **SalaryDecisionDeclineEvent**
## Current node: **AgeDecisionNode**
## Published event: **AgeDecisionAcceptEvent**
## Finish

## Start
## Loan identifier=2, Age=21, Sex=M, Marriage=true, Salary=3000, Profession=IT
## Current node: **ProfessionDecisionNode**
## Published event: **ProfessionDecisionAcceptEvent**
## Current node: **SalaryDecisionNode**
## Published event: **SalaryDecisionAcceptEvent**
## Current node: **MarriageDecisionNode**
## Published event: **MarriageDecisionAcceptEvent**
## Finish

## Start
## Loan identifier=3, Age=18, Sex=M, Marriage=true, Salary=1000, Profession=HR
## Current node: **ProfessionDecisionNode**
## Published event: **ProfessionDecisionDeclineEvent**
## Current node: **SexDecisionNode**
## Published event: **SexDecisionAcceptEvent**
## Current node: **AgeDecisionNode**
## Published event: **AgeDecisionDeclineEvent**
## Finish

## Start
## Loan identifier=1, Age=21, Sex=F, Marriage=false, Salary=1000, Profession=IT
## Current node: **ProfessionDecisionNode**
## Published event: **ProfessionDecisionAcceptEvent**
## Current node: **SalaryDecisionNode**
## Published event: **SalaryDecisionDeclineEvent**
## Current node: **AgeDecisionNode**
## Published event: **AgeDecisionAcceptEvent**
## Finish


## Disconnected from the target VM, address: '127.0.0.1:37279', transport: 'socket'

## Process finished with exit code 0


